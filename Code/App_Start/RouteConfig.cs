﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Code
{
    public class RouteConfig
    {
        /// <summary>
        /// The method for Register custome route
        /// </summary>
        /// <param name="routes">The routes.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "login", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
