﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace Code.Controllers
{
    /// <summary>
    /// Login/Secure version controller
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class LoginController : Controller
    {
        /// <summary>
        /// This is get method for index page 
        /// </summary>
        /// <returns>Return index/login view for the page</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This is a post method for index/login page. 
        /// Before excuting method, validate the forgery token to avoid the CSRF. 
        /// This method has code to validate user credentials.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns>Return view for the page</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName.Trim()) || string.IsNullOrEmpty(password.Trim()))
            {
                ModelState.AddModelError("Error", "Please enter valid credentials.");
            }
            else
            {
                var hashPassword = GetHashPassword(userName);

                if (!string.IsNullOrEmpty(hashPassword))
                {
                    // Validate the password with real hash password
                    bool IsValidPassword = PasswordHash.ValidatePassword(password, hashPassword);

                    if (IsValidPassword)
                    {
                        //Display this message if, credentials are valid.
                        ViewBag.success = "Your credentials are valid";
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Please enter valid credentials.");
                    }
                }
                else
                {
                    // Validate the password with wrong hash password to avoid timing attack
                    PasswordHash.ValidatePassword(password, "1000:AH3W4eEIA8OG/t/6DxphjYXKyZzf7gaC:zt/EAoK8JGqHMDPs5nM80GgEilSKnXoJ");

                    ModelState.AddModelError("Error", "Please enter valid credentials.");
                }
            }

            return View();
        }

        /// <summary>
        /// Gets the real hash password of the user by userName.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>It return hash password if user exists, otherwise null</returns>
        private string GetHashPassword(string userName)
        {   
            using (var sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var sqlSelectQuery = "SELECT top 1 HashPassword FROM Users where UserName=@userName";

                var sqlCommand = new SqlCommand(sqlSelectQuery, sqlCon);

                // using parameterized query to avoid sql injection
                sqlCommand.Parameters.AddWithValue("@userName", userName);

                sqlCon.Open();

                return Convert.ToString(sqlCommand.ExecuteScalar());
            }
        }

    }
}