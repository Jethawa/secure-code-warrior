﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Code
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Appliction start event.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
